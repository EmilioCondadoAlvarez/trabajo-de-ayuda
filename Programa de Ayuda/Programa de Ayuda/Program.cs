﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa_de_Ayuda
{
    class Program
    {
        static void Main(string[] args)
        {
            string empleado;
            Console.WriteLine("Este Programa Nos Mostrara El Pago De Los Diferentes Empleados\n");
            Console.WriteLine("Escriba Que Tipo De Empleado Es Usted:\n");
            Console.WriteLine("Gerente\n");
            Console.WriteLine("Empleado Por Hora\n");
            Console.WriteLine("Empleado Por Comision\n");
            Console.WriteLine("Empleado Por Destajo\n");
            empleado = Console.ReadLine();
            switch (empleado)
            {
                case "Gerente":
                    ventana1.gerente();
                    break;
                case "Empleado Por Hora":
                    ventana2.Empleado_Por_Hora();
                    break;
            }
        }
    }
}
